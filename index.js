function displayMsgToSelf(){
	console.log("Hello.")
};

// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();

let count = 10;

while(count !== 0){
	displayMsgToSelf();
	count--;
};

// While Loop
	// While loop will allow us to repeat an instruction as long as the condition is true.
	/*
		Syntax:
		while(condition is true){
			statement/instruction runs incrementation or decrementation to stop the loop.
		};
	*/

let number = 5;
//countdown from 5-1

// While the value of count is not equal to 0,
while(number !== 0){

	// The current value will be printed out.
	console.log("While " + number);

	// Decreases the count by 1 after every iteration to stop the loop when it reaches zero.
	number--;
};

number = 5
// count from 5-10
while(number <= 10){
	console.log(`While ${number}`);
	number++;
};

// Do-While Loop
	// A do-while loop works alot like a while loop, but, unlike a while loop, do-while guarantees to run at least once.
	/*
		Syntax:
		do{
			statement
		} while(condition)
	*/
	
	// Number function works like parstInt, it changes a string type to a number data type.
	let number2 = Number(prompt("Give me a number."))

	do{
		// The current value of the number2 is printed out.
		console.log(`Do While ${number2}`);

		// increases the number by 1 after every iteration of the loop
		number2 += 1;

	} while(number2 < 10);

	let counter = 1

	do {
		console.log(counter);
		counter ++;

	} while(counter <= 21);

// For Loop
	// A more flexible loop than while and do-while loop.
	// It consists fo three parts: initialization, condition, finalExpression
		// initialization - determines where the loop starts
		// condition - determines when the loop stops
		// finalExpression - indicates how to advance he loop (incrementation or decrementation)
	/*
		Syntax:
		for(initialization; condition; finalExpression){
			statement;
		}
	*/

	for(let count = 0; count <= 20; count++){
		console.log(count)
	};

	let myString = "alex"
	console.log(myString.length)
		// result: 4
	// Note: Strings are special compared to other data types because it has access to functions and other pieces of information that another primitive data type might not have.

	// myString = ["a", "l", "e", "x"]

	console.log(myString[0]);
		// result: a
	console.log(myString[3]);
		// result: x

	for(let x = 0; x < myString.length; x++){
		console.log(myString[x]);
	};

	let myName = "Jason"

	for(let i = 0; i < myName.length; i++){
		if(
			myName[i].toLowerCase() == "a" ||
			myName[i].toLowerCase() == "e" ||
			myName[i].toLowerCase() == "i" ||
			myName[i].toLowerCase() == "o" ||
			myName[i].toLowerCase() == "u"
		){
			console.log(3)

		} else {
			console.log(myName[i])
		}
	};

// Continue and Break Statements
	// continue resets the loop, not running anything underneath it.
	// break stops the loop.

	for(let count = 0; count <= 20; count++){
		if(count % 2 === 0){
			continue;
		}

		console.log(`Continue and Break ${count}`)

		if(count > 10){
			break;
		}
	}

	let name = "alexandro";

	for(let i = 0; i < name.length; i++){
		console.log(name[i]);

		if(name[i].toLowerCase() === "a"){
			console.log("Continue to the next iteration");
			continue;
		}

		if(name[i] == "d"){
			break;
		}
	}